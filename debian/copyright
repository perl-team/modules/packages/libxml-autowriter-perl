Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: XML-AutoWriter
Upstream-Contact: Chris Prather <chris@prather.org>
Source: https://metacpan.org/release/XML-AutoWriter

Files: *
Copyright: 2000-2009, Barrie Slaymaker <barries@slaysys.com>
License: Artistic or GPL-1+
Comment: It is not clear which BSD flavour the authors had in mind; so we
 distribute the package under GPL/Artistic, like Perl itself and most Perl
 modules.

Files: inc/Module/*
Copyright:
 2002-2009, Adam Kennedy <adamk@cpan.org>
 2002-2009, Audrey Tang <autrijus@autrijus.org>
 2002-2009, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2004-2008, Piotr Roszatycki <dexter@debian.org>
 2008, Damyan Ivanov <dmn@debian.org>
 2009, Jonathan Yu <jawnsy@cpan.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
